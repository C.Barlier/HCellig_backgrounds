# HCellig Hierarchical Backgrounds

**This repository is part of a project, linked to the two following repositories:**

**- HCellig R Package: https://github.com/BarlierC/HCellig**

**- HCellig analyses: https://gitlab.com/C.Barlier/HCellig_analyses**

Hierarchical layers have been pre-compiled for Mouse [1] and Human [2] **to characterize cell identity using HCellig**. In addition, a neuron subtypes background and several neuron phenotype backgrounds have been generated using the Mouse Brain Atlas [3].  

This repository contains **cell type backgrounds, cell subtype backgrounds and phenotypes backgrounds for Mouse and Human.**

*[1] Single-cell transcriptomics of 20 mouse organs creates a Tabula Muris, The Tabula Muris Consortium et al. - Nature, 2018*

*[2] The Tabula Sapiens: A multiple-organ, single-cell transcriptomic atlas of humans, The Tabula Sapiens Consortium - Science, 2022*

*[3] La Manno, G. et al. Nature 596, 92–96 (2021)*
